#!/bin/bash
set -ex

vloume_base=/data-volume/mysql

if [ ! -d ${vloume_base}/config ]; then
    mkdir -p ${vloume_base}/config
fi
if [ ! -d ${vloume_base}/config/conf.d ]; then
    mkdir -p ${vloume_base}/config/conf.d
fi
if [ ! -d ${vloume_base}/data ]; then
    mkdir -p ${vloume_base}/data
fi
if [ ! -d ${vloume_base}/log ]; then
    mkdir -p ${vloume_base}/log
fi

charset_cnf_url="https://gitlab.com/palms/ops/ops-config-repo/raw/master/mysql/charset.cnf"
timezone_cnf_url="https://gitlab.com/palms/ops/ops-config-repo/raw/master/mysql/timezone.cnf"

curl -fSL ${charset_cnf_url} -o ${vloume_base}/config/conf.d/charset.cnf
curl -fSL ${timezone_cnf_url} -o ${vloume_base}/config/conf.d/timezone.cnf