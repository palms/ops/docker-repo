#!/bin/bash

# 错误就退出
set -e
# 开启调试模式
set -x

bin=$(dirname "$0")
BIN=$(cd "$bin"; pwd)
export DR_IMAGE_DIR="$BIN"
export DR_HOME=$(cd "$BIN/../.."; pwd)

source "$DR_HOME/common/scripts/project-tools-common.sh"

lang=java
git_repository=git@gitlab.com:humanness/duka.git

set_git_ssh
init "$git_repository"

debug(){
  git_repository="$1"
  check "$git_repository"

  app_name=$(get_app_name "$git_repository")
  check "$app_name"

  if [ $(docker image ls --format {{.Repository}} | grep "hellomrpc/${app_name}" | wc -l) -eq 0 ]; then
     printf "docker image hellomrpc/${app_name} is not exist\n"
     exit 1
  fi
  
  docker run -it --rm --name "${app_name}" \
  --network host --expose 1025-65534 \
  -e SPRING_PROFILES_ACTIVE=dev \
  "hellomrpc/${app_name}" /bin/bash '-c' 'java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005 -jar /root/${TARGET_NAME}'
}


case "$1" in
build)
build "$lang" "$git_repository"
;;
start)
stop "$git_repository"
start "$git_repository"
;;
stop)
stop "$git_repository"
;;
test)
test "$git_repository"
;;
debug)
debug "$git_repository"
;;
*)
printf 'Usage: {build|start|stop|test|debug}\n'
exit 1
;;
esac