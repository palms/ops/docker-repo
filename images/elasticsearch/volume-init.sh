#!/bin/bash
set -ex

vloume_base=/data-volume/elasticsearch

if [ ! -d ${vloume_base}/config ]; then
    mkdir -p ${vloume_base}/config
fi
if [ ! -d ${vloume_base}/data ]; then
    mkdir -p ${vloume_base}/data
fi
if [ ! -d ${vloume_base}/log ]; then
    mkdir -p ${vloume_base}/log
fi
if [ ! -d ${vloume_base}/plugins ]; then
    mkdir -p ${vloume_base}/plugins
fi
chown -R 1000:1000 ${vloume_base}