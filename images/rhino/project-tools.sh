#!/bin/bash

# 错误就退出
set -e
# 开启调试模式
set -x

bin=$(dirname "$0")
BIN=$(cd "$bin"; pwd)
export DR_IMAGE_DIR="$BIN"
export DR_HOME=$(cd "$BIN/../.."; pwd)

source "$DR_HOME/common/scripts/project-tools-common.sh"

lang=java
git_repository=git@gitlab.com:humanness/rhino.git

init "$git_repository"

case "$1" in
fe_install)
front_end_install "$git_repository"
;;
fe_build)
front_end_build "$git_repository"
;;
*)
printf 'Usage: {fe_install|fe_build}\n'
exit 1
;;
esac