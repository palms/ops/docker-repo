#!/bin/bash

# 错误就退出
set -e
# 开启调试模式
set -x

bin=$(dirname "$0")
BIN=$(cd "$bin"; pwd)
export DR_IMAGE_DIR="$BIN"
export DR_HOME=$(cd "$BIN/../.."; pwd)

source "$DR_HOME/common/scripts/project-tools-common.sh"

lang=java
git_repository=git@gitlab.com:humanness/cow.git

init "$git_repository"

case "$1" in
build)
build "$lang" "$git_repository"
;;
start)
stop "$git_repository"
start "$git_repository"
;;
stop)
stop "$git_repository"
;;
test)
test "$git_repository"
;;
*)
printf 'Usage: {build|start|stop|test}\n'
exit 1
;;
esac