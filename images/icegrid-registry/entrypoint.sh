#!/bin/bash
set -e

if [ "$1" = "namenode" ]; then
  if [ ! -d /data-volume/hadoop/data/namenode ]; then
    mkdir -p /data-volume/hadoop/data/namenode
  fi
  if [ $(ls -A /data-volume/hadoop/data/namenode | wc -w) -eq 0 ]; then
    hdfs namenode -format ${CLUSTER_NAME}
  fi
  exec hdfs namenode
elif [ "$1" = "resourcemanager" ]; then
  exec yarn resourcemanager
elif [ "$1" = "datanode" ]; then
  exec hdfs datanode
elif [ "$1" = "nodemanager" ]; then
  exec yarn nodemanager
else
  exec "$@"
fi