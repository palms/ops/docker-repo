#!/bin/bash
set -ex

vloume_base=/data-volume/icegrid-registry

if [ ! -d ${vloume_base}/config ]; then
    mkdir -p ${vloume_base}/config
fi
if [ ! -d ${vloume_base}/data ]; then
    mkdir -p ${vloume_base}/data
fi
if [ ! -d ${vloume_base}/log ]; then
    mkdir -p ${vloume_base}/log
fi

registry_cfg_url="https://gitlab.com/palms/ops/ops-config-repo/raw/master/icegrid-registry/registry.cfg"
curl -fSL ${registry_cfg_url} -o ${vloume_base}/config/registry.cfg