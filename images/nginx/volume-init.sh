#!/bin/bash
set -ex

vloume_base=/data-volume/nginx

if [ ! -d ${vloume_base}/config ]; then
    mkdir -p ${vloume_base}/config
fi
if [ ! -d ${vloume_base}/config/conf.d ]; then
    mkdir -p ${vloume_base}/config/conf.d
fi
if [ ! -d ${vloume_base}/data ]; then
    mkdir -p ${vloume_base}/data
fi
if [ ! -d ${vloume_base}/data/html ]; then
    mkdir -p ${vloume_base}/data/html
fi
if [ ! -d ${vloume_base}/log ]; then
    mkdir -p ${vloume_base}/log
fi

nginx_conf_url="https://gitlab.com/palms/ops/ops-config-repo/raw/master/nginx/nginx.conf"
confd_default_conf_url="https://gitlab.com/palms/ops/ops-config-repo/raw/master/nginx/conf.d/default.conf"
confd_hellomrpc_conf_url="https://gitlab.com/palms/ops/ops-config-repo/raw/master/nginx/conf.d/hellomrpc.conf"
html_index_url="https://gitlab.com/palms/ops/ops-config-repo/raw/master/nginx/html/index.html"
html_50x_url="https://gitlab.com/palms/ops/ops-config-repo/raw/master/nginx/html/50x.html"

curl -fSL ${nginx_conf_url} -o ${vloume_base}/config/nginx.conf
curl -fSL ${confd_default_conf_url} -o ${vloume_base}/config/conf.d/default.conf
curl -fSL ${confd_hellomrpc_conf_url} -o ${vloume_base}/config/conf.d/hellomrpc.conf
curl -fSL ${html_index_url} -o ${vloume_base}/data/html/index.html
curl -fSL ${html_50x_url} -o ${vloume_base}/data/html/50x.html