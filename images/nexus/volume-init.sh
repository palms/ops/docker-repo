#!/bin/bash
set -ex

vloume_base=/data-volume/nexus

if [ ! -d ${vloume_base}/config ]; then
    mkdir -p ${vloume_base}/config
fi
if [ ! -d ${vloume_base}/data ]; then
    mkdir -p ${vloume_base}/data
fi
if [ ! -d ${vloume_base}/log ]; then
    mkdir -p ${vloume_base}/log
fi
chown -R 200 ${vloume_base}