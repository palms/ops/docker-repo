#!/bin/bash
set -e

if [ "$1" = "master" ]; then
  exec hbase master start
elif [ "$1" = "regionserver" ]; then
  exec hbase regionserver start
else
  exec "$@"
fi