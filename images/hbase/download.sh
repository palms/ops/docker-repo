#!/bin/bash

# 错误就退出
set -e
# 开启调试模式
set -x

bin=`dirname "$0"`
BIN=`cd "$bin"; pwd`
export DR_IMAGE_DIR="$BIN"

HBASE_VERSION=2.0.1
URL="http://www.apache.org/dist/hbase/${HBASE_VERSION}/hbase-${HBASE_VERSION}-bin.tar.gz"
FILE="${DR_IMAGE_DIR}/hbase.tar.gz"

curl -fSL "$URL" -o "$FILE""
