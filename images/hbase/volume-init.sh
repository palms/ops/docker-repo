#!/bin/bash
set -ex

vloume_base=/data-volume/hbase

if [ ! -d ${vloume_base}/config ]; then
    mkdir -p ${vloume_base}/config
fi
if [ ! -d ${vloume_base}/data ]; then
    mkdir -p ${vloume_base}/data
fi
if [ ! -d ${vloume_base}/log ]; then
    mkdir -p ${vloume_base}/log
fi

hbase_site_url="https://gitlab.com/palms/ops/ops-config-repo/raw/master/hbase/hbase-site.xml"
curl -fSL ${hbase_site_url} -o ${vloume_base}/config/hbase-site.xml