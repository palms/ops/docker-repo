#!/bin/bash

# 错误就退出
set -e
# 开启调试模式
set -x

bin=`dirname "$0"`
BIN=`cd "$bin"; pwd`
export DR_IMAGE_DIR="$BIN"

HADOOP_VERSION=2.9.1
URL="http://www.apache.org/dist/hadoop/common/hadoop-${HADOOP_VERSION}/hadoop-${HADOOP_VERSION}.tar.gz"
FILE="${DR_IMAGE_DIR}/hadoop.tar.gz"

curl -fSL "$URL" -o "$FILE""
