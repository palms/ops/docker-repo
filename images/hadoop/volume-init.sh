#!/bin/bash
set -ex

vloume_base=/data-volume/hadoop

if [ ! -d ${vloume_base}/config ]; then
    mkdir -p ${vloume_base}/config
fi
if [ ! -d ${vloume_base}/data ]; then
    mkdir -p ${vloume_base}/data
fi
if [ ! -d ${vloume_base}/log ]; then
    mkdir -p ${vloume_base}/log
fi

core_site_url="https://gitlab.com/palms/ops/ops-config-repo/raw/master/hadoop/core-site.xml"
hdfs_site_url="https://gitlab.com/palms/ops/ops-config-repo/raw/master/hadoop/hdfs-site.xml"
yarn_site_url="https://gitlab.com/palms/ops/ops-config-repo/raw/master/hadoop/yarn-site.xml"
mapred_site_url="https://gitlab.com/palms/ops/ops-config-repo/raw/master/hadoop/mapred-site.xml"

curl -fSL ${core_site_url} -o ${vloume_base}/config/core-site.xml
curl -fSL ${hdfs_site_url} -o ${vloume_base}/config/hdfs-site.xml
curl -fSL ${yarn_site_url} -o ${vloume_base}/config/yarn-site.xml
curl -fSL ${mapred_site_url} -o ${vloume_base}/config/mapred-site.xml