#!/bin/bash
set -ex

vloume_base=/data-volume/rabbitmq

if [ ! -d ${vloume_base}/config ]; then
    mkdir -p ${vloume_base}/config
fi
if [ ! -d ${vloume_base}/data ]; then
    mkdir -p ${vloume_base}/data
fi
if [ ! -d ${vloume_base}/log ]; then
    mkdir -p ${vloume_base}/log
fi

enabled_plugins_url="https://gitlab.com/palms/ops/ops-config-repo/raw/master/rabbitmq/enabled_plugins"
rabbitmq_conf_url="https://gitlab.com/palms/ops/ops-config-repo/raw/master/rabbitmq/rabbitmq.conf"

curl -fSL ${enabled_plugins_url} -o ${vloume_base}/config/enabled_plugins
curl -fSL ${rabbitmq_conf_url} -o ${vloume_base}/config/rabbitmq.conf