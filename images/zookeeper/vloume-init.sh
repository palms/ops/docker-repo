#!/bin/bash
set -ex

vloume_base=/data-volume/zookeeper

if [ ! -d ${vloume_base}/config ]; then
    mkdir -p ${vloume_base}/config
fi
if [ ! -d ${vloume_base}/data ]; then
    mkdir -p ${vloume_base}/data
fi
if [ ! -d ${vloume_base}/log ]; then
    mkdir -p ${vloume_base}/log
fi

zoo_cfg_url="https://gitlab.com/palms/ops/ops-config-repo/raw/master/zookeeper/zoo.cfg"

curl -fSL ${zoo_cfg_url} -o ${vloume_base}/config/zoo.cfg