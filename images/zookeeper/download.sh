#!/bin/bash

# 错误就退出
set -e
# 开启调试模式
set -x

bin=`dirname "$0"`
BIN=`cd "$bin"; pwd`
export DR_IMAGE_DIR="$BIN"

ZOOKEEPER_VERSION=3.4.13
URL="http://www-us.apache.org/dist/zookeeper/zookeeper-${ZOOKEEPER_VERSION}/zookeeper-${ZOOKEEPER_VERSION}.tar.gz"
FILE="${DR_IMAGE_DIR}/zookeeper.tar.gz"

curl -fSL "$URL" -o "$FILE""
