#!/bin/bash
set -e

# Write myid only if it doesn't exist
if [ ! -f "/data-volume/zookeeper/data/myid" ]; then
    if [ "$MY_ID" == "" ]; then
      printf "myid can not be found"
      exit 1
    fi
    if [ ! -d /data-volume/zookeeper/data ]; then
      mkdir -p /data-volume/zookeeper/data
    fi
    echo "$MY_ID" > "/data-volume/zookeeper/data/myid"
fi

if [ "$1" = "zookeeper" ]; then
  exec zkServer.sh start-foreground
else
  exec "$@"
fi