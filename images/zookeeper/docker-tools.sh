#!/bin/bash

# 错误就退出
set -e
# 开启调试模式
set -x

bin=`dirname "$0"`
BIN=`cd "$bin"; pwd`
export DR_IMAGE_DIR="$BIN"
export DR_HOME=$(cd "$BIN/../.."; pwd)

source "$DR_HOME/common/scripts/docker-tools-common.sh"

case "$1" in
build)
build
;;
push)
push
;;
*)
printf 'Usage: {build|push}\n'
exit 1
;;
esac