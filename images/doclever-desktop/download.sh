#!/bin/bash

# 错误就退出
set -e
# 开启调试模式
set -x

bin=`dirname "$0"`
BIN=`cd "$bin"; pwd`
export DR_IMAGE_DIR="$BIN"

URL="http://oss.hellomrpc.com/docker-repository/images/doclever-desktop/doclever-desktop.tar.gz"
FILE="${DR_IMAGE_DIR}/doclever-desktop.tar.gz"

curl -fSL "$URL" -o "$FILE""
