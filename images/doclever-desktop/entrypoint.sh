#!/bin/bash
set -e

DOClever_MONGO=${DOClever_MONGO:-"mongodb://localhost:27017/DOClever"}
DOClever_FILEPATH=${DOClever_FILEPATH:-"/Users/Shared/DOClever"}
DOClever_PORT=${DOClever_PORT:-"48963"}

echo "{\"db\":\"${DOClever_MONGO}\",\"filePath\":\"${DOClever_FILEPATH}\",\"port\":${DOClever_PORT}}" > /root/doclever-desktop/DOClever-Server/config.json

exec "$@"