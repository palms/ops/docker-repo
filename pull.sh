#!/bin/bash
set -ex
bin=$(dirname "$0")
BIN=$(cd "$bin"; pwd)
file=$(basename "$0")
cmd="alias drpull=${BIN}/${file}"
echo "$cmd"
cd "$BIN"
git config --local core.fileMode false
git pull
find . -type f  -name "*.sh" | xargs chmod +x