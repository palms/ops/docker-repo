#!/bin/bash
apt-get update && apt-get install -y vim && perl -pi -e 's/mouse=/mouse-=/g' /usr/share/vim/vim80/defaults.vim
echo "export LS_OPTIONS='--color=auto'
alias ls='ls \$LS_OPTIONS'
alias ll='ls \$LS_OPTIONS -l'
alias l='ls \$LS_OPTIONS -lA'
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias docker-enter='f() { docker exec -it \$1 /bin/bash;}; f'" > ~/.bashrc
source ~/.bashrc