#!/bin/bash
if [ "$DR_HOME" == "" ]; then
 printf "DR_HOME envirement variable is not exist\n"
 exit 1
fi
source "$DR_HOME/common/scripts/check.sh"

# param git_repository
init(){
  check "$DR_SRC_DIR"
  check "$DR_IMAGE_DIR"

  git_repository="$1"
  check "$git_repository"

  app_name=$(get_app_name "$git_repository")
  check "$app_name"

  if [ ! -d "${DR_SRC_DIR}" ]; then
    mkdir "${DR_SRC_DIR}"
  fi
  if [ ! -d "${DR_SRC_DIR}/${app_name}" ]; then
    cd "${DR_SRC_DIR}"
    git clone "$git_repository"
  fi
  if [ ! -d "$DR_IMAGE_DIR/target" ]; then
    mkdir "$DR_IMAGE_DIR/target"
  fi
}

# param git_repository
get_app_name(){
  git_repository="$1"
  check "$git_repository"
  var1=${git_repository##*/}
  var2=${var1%.*}
  echo "$var2"
}

get_repository_name(){
    app_name="$1"
    check "$app_name"
    echo "hellomrpc/${app_name}"
}

# param lang app_name
get_target_name(){
  lang="$1"
  check "$lang"
  
  app_name="$2"
  check "$app_name"

  if [ "$lang" == "java" ]; then
    echo "${app_name}.jar"
  elif [ "$lang" == "go" ]; then
    echo "${app_name}"
  else
    printf "todo\n"
  fi
}

# param lang app_name
move_target(){
  check "$DR_SRC_DIR"
  check "$DR_IMAGE_DIR"

  lang="$1"
  check "$lang"
  
  app_name="$2"
  check "$app_name"

  target_name=$(get_target_name $lang $app_name)
  check "$target_name"

  if [ $lang == "java" ]; then
    mv "${DR_SRC_DIR}/${app_name}/target/${target_name}" "${DR_IMAGE_DIR}/target/${target_name}" 
  elif [ $lang == "go" ]; then
    mv "${DR_SRC_DIR}/${app_name}/${target_name}" "${DR_IMAGE_DIR}/target/${target_name}"
  else
    printf "todo\n"
  fi
}

# param lang git_repository
build(){
  check "$DR_SRC_DIR"
  check "$DR_IMAGE_DIR"

  lang="$1"
  check "$lang"
  
  git_repository="$2"
  check "$git_repository"

  app_name=$(get_app_name "$git_repository")
  check "$app_name"

  target_name=$(get_target_name $lang $app_name)
  check "$target_name"

  if [ ! -d "${DR_SRC_DIR}/${app_name}" ]; then
    printf "please init project first\n"
    exit 1
  fi
  
  # git pull
  cd "${DR_SRC_DIR}/${app_name}"
  git pull
  
  # build
  cd "$DR_IMAGE_DIR"
  if [ $lang == "java" ]; then
    docker run -it --rm \
    -v "${DR_SRC_DIR}/${app_name}:/usr/src/java" \
    -v /data-volume/maven:/root/.m2 \
    -w /usr/src/java \
    maven:3.5.4-jdk-8 \
    mvn clean package    
  elif [ $lang == "go" ]; then
    docker run -it --rm \
    -v "${DR_SRC_DIR}/${app_name}:/go/src/hellomrpc/${app_name}" \
    -w "/go/src/hellomrpc/${app_name}" \
    golang:1.10.3-stretch go build -v
  else
    printf "todo\n"
  fi
  
  # move target file
  move_target "$lang" "$app_name"
  
  repository_name=$(get_repository_name "$app_name")

  # stop older docker container
  if [ $(docker ps --format '{{.Image}} {{.Names}}' | grep "${repository_name}" | wc -l) -gt 0 ]; then
      docker ps --format '{{.Image}} {{.Names}}' | grep "${repository_name}" | awk '{print $2}' | xargs docker stop 
  fi

  # remove older docker container
  if [ $(docker container ls -a --format '{{.Image}} {{.Names}}' | grep "${repository_name}" | wc -l) -gt 0 ]; then
      docker container ls -a --format '{{.Image}} {{.Names}}' | grep "${repository_name}" | awk '{print $2}' | xargs docker rm
  fi

  # remove older docker image
  if [ $(docker image ls | grep "${repository_name}" | wc -l) -gt 0 ]; then
      docker image ls | grep "${repository_name}" | awk '{print $3}' | xargs docker rmi
  fi

  # docker build
  if [ ! -f "Dockerfile-$lang" ]; then
     printf "Dockerfile-$lang is not exist\n"
     exit 1
  fi
  
  docker build -t "${repository_name}" \
   -f "Dockerfile-$lang" \
   --build-arg TARGET_NAME="${target_name}" \
   .
  
  
}

# param git_repository
stop(){
  git_repository="$1"
  check "$git_repository"

  app_name=$(get_app_name "$git_repository")
  check "$app_name"

  if [ $(docker ps --format "{{.Names}}" | grep ${app_name} | wc -l) -gt 0 ]; then
      docker stop "${app_name}"
  fi
}

# param git_repository
start() {
  git_repository="$1"
  check "$git_repository"

  app_name=$(get_app_name "$git_repository")
  check "$app_name"

  if [ $(docker container ls -a --format "{{.Names}}" | grep ${app_name} | wc -l) -eq 0 ]; then
      docker create --name "${app_name}" \
      --restart always \
      --network host --expose 1025-65534 \
      -e SPRING_PROFILES_ACTIVE=${SPRING_PROFILES_ACTIVE} \
      --log-driver json-file --log-opt max-size=10m \
      "hellomrpc/${app_name}"
  fi
  docker start "${app_name}"
}

# param git_repository
test(){
  git_repository="$1"
  check "$git_repository"

  app_name=$(get_app_name "$git_repository")
  check "$app_name"

  if [ $(docker image ls --format {{.Repository}} | grep "hellomrpc/${app_name}" | wc -l) -eq 0 ]; then
     printf "docker image hellomrpc/${app_name} is not exist\n"
     exit 1
  fi
  
  docker run -it --rm --name "${app_name}" \
  --network host --expose 1025-65534 \
  -e SPRING_PROFILES_ACTIVE=dev \
  "hellomrpc/${app_name}"
}

# front_end_install
front_end_install(){
  check "$DR_SRC_DIR"

  git_repository="$1"
  check "$git_repository"

  app_name=$(get_app_name "$git_repository")
  check "$app_name"

  # git pull
  cd "${DR_SRC_DIR}/${app_name}"
  git pull

  if [ -d "${DR_SRC_DIR}/${app_name}/node_modules" ]; then
    rm -rf "${DR_SRC_DIR}/${app_name}/node_modules"
  fi

  docker run -it --rm --name node-builder \
  -v "${DR_SRC_DIR}/${app_name}":/usr/src/app \
  -w /usr/src/app \
  node:8 \
  npm install

}

# front_end_build
front_end_build(){
  check "$DR_SRC_DIR"
  check "$HTML_ROOT_DIR"

  git_repository="$1"
  check "$git_repository"

  app_name=$(get_app_name "$git_repository")
  check "$app_name"

  # git pull
  cd "${DR_SRC_DIR}/${app_name}"
  git pull
  
  if [ -d "${DR_SRC_DIR}/${app_name}/dist" ]; then
    rm -rf "${DR_SRC_DIR}/${app_name}/dist"  
  fi

  docker run -it --rm --name node-builder \
  -v "${DR_SRC_DIR}/${app_name}":/usr/src/app \
  -w /usr/src/app \
  node:8 \
  npm run build

  if [ -d "${HTML_ROOT_DIR}/${app_name}" ]; then
    rm -rf "${HTML_ROOT_DIR}/${app_name}"  
  fi

  mv "${DR_SRC_DIR}/${app_name}/dist" "${HTML_ROOT_DIR}/${app_name}"
}

# set git ssh
set_git_ssh() {
    check "$SSH_KEY"
    echo "ssh -i $SSH_KEY \$@" > /tmp/.git_ssh.$$
    chmod +x /tmp/.git_ssh.$$
    export GIT_SSH=/tmp/.git_ssh.$$
}